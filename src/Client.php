<?php
/**
 * Author: Mauricio van der Maesen de Sombreff
 * email: mauricio.van.der.maesen@iqu.com
 * File: Client.php
 * Date: 18/12/2017
 */

require __DIR__ . '/../vendor/autoload.php';

use \JsonRpc\Client;

$client = new Client('http://127.0.0.1:9999/Server.php');

$client->call('testMethod1', ['data' => ['a' => 1, 'b' => 2]]);
$result = $client->result;

var_export($result);
echo PHP_EOL;

$client->call('add', [1,2,3,4,5,6,7,8,9]);
$result = $client->result;

var_export($result);
echo PHP_EOL;
