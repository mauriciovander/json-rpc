<?php
/**
 * Author: Mauricio van der Maesen de Sombreff
 * email: mauricio.van.der.maesen@iqu.com
 * File: server.php
 * Date: 18/12/2017
 */

ini_set('default_charset', 'UTF-8');
ini_set('display_errors', '0');

require __DIR__.'/../vendor/autoload.php';

use Mauriciovander\JsonRpc\Methods;

header('Content-Type: application/json');
$methods = new Methods();
$logger = new \Monolog\Logger('server-log');
$server = new JsonRpc\Server($methods);
$server->setLogger($logger);
$server->receive();

