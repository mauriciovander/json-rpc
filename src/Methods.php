<?php
/**
 * Author: Mauricio van der Maesen de Sombreff
 * email: mauricio.van.der.maesen@iqu.com
 * File: Methods.php
 * Date: 18/12/2017
 */

namespace Mauriciovander\JsonRpc;

class Methods
{
    public $error = null;

    public function testMethod1($data)
    {
        return $data;
    }

    public function add()
    {
        $result = 0;
        foreach (func_get_args() as $number){
            $result += floatval($number);
        }
        return $result;
    }
}
