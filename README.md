
## Run the server:
```
cd src
php -S 127.0.0.1:9999
```
 
## Test:  
```
curl -X POST \
  http://127.0.0.1:9999/Server.php \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"jsonrpc": "2.0", 
	"method": "testMethod1", 
	"params": {
		"data":1234
	},
	"id": 0
}'
```

```
curl -X POST \
  http://127.0.0.1:9999/Server.php \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"jsonrpc": "2.0", 
	"method": "add", 
	"params": [1,2,4,6,23],
	"id": 1000
}'
```

Should return:
```
{"jsonrpc":"2.0","result":36,"id":1000}
```

## Use Client

```
php src/Client.php
```
